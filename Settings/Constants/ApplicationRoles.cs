﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Settings.Constants
{
    public class ApplicationRoles
    {
        public const string HELPER = "HELPER";
        public const string GUARANTOR = "GUARANTOR";
        public const string EMPLOYER = "EMPLOYER";
        public const string ADMIN = "ADMIN";
        public const string SUPER_ADMIN = "SUPER_ADMIN";

    }
    
}
