﻿
using Service.DTO_s;

namespace Service.Interface
{
    public interface IEmployer : IBaseRepository<EmployerDTO>
    {
    }
}
